#Anastassia Bobokalonova
#April 1, 2017


# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  # uppercase = ""
  # str.each_char do |ch|
  #   next if ch == ch.downcase
  #   uppercase << ch
  # end
  # uppercase
  #OR:
  lowercase = "abcdefghijklmnopqrstuvwxyz"
  str.delete(lowercase)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[((str.length / 2) - 1)..str.length / 2]
  else
    str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  VOWELS.each do |vowel|
    count += str.count(vowel)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each do |el|
    joined << el + separator
  end
  joined[0..((-1 * separator.length) - 1)]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.chars.each_with_index do |ch, i|
    if i.even?
      weird_str << ch.downcase
    else
      weird_str << ch.upcase
    end
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  reversed = []
  words.each do |word|
    if word.length >= 5
      reversed << word.reverse
    else
      reversed << word
    end
  end
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |int|
    if int % 5 == 0
      if int % 3 == 0
        arr << "fizzbuzz"
      else
        arr << "buzz"
      end
    elsif int % 3 == 0
      arr << "fizz"
    else
      arr << int
    end
  end

  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.each do |el|
    reverse_arr.unshift(el)
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num / 2).each do |int|
    return false if num % int == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  (1..num).each do |int|
    factors_arr << int if num % int == 0
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  # prime_factors_arr = []
  # (1..num).each do |int|
  #   prime_factors_arr << int if (num % int == 0) && prime?(int)
  # end
  # prime_factors_arr
  # OR:
  all_factors = factors(num)
  all_factors.select {|f| prime?(f)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr[0].even? && arr[1].even?
    arr.select {|int| int.odd?}
  elsif arr[0].odd? && arr[1].odd?
    arr.select {|int| int.even?}
  else #If the first two elements have different parities, one is the oddball.
    if arr[2].even?
      return arr[1] if arr[1].odd?
      return arr[0] if arr[0].odd?
    else
      return arr[1] if arr[1].even?
      return arr[0] if arr[0].even?
    end
  end
end
